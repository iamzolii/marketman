/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

/**
 *
 * @author bzolt
 */
import facade.MarketOrderFacade;
import facade.PartnerFacade;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import model.MarketOrder;
import model.Partner;

@FacesConverter(value = "orderConverter")
public class OrderConverter implements Converter {
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return null;
        }
        MarketOrder order = (MarketOrder) value;
        String idAsString = String.valueOf(order.getId());
        return idAsString;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
                return null;
        }
        Integer id = Integer.valueOf(value);
        MarketOrder order = null;
        try {
            MarketOrderFacade orderFacade = (MarketOrderFacade) new InitialContext().lookup("java:module/MarketOrderFacade");
            order = orderFacade.findById(id);
        } catch (NamingException ex) {
            Logger.getLogger(EmployeeConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return order;
    }

}
