/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

/**
 *
 * @author bzolt
 */
import facade.DriverFacade;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import model.Driver;

@FacesConverter(value = "driverConverter")
public class DriverConverter implements Converter {
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return null;
        }
        Driver driver = (Driver) value;
        String idAsString = String.valueOf(driver.getId());
        return idAsString;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
                return null;
        }
        Integer id = Integer.valueOf(value);
        Driver driver = null;
        try {
            DriverFacade driverFacade = (DriverFacade) new InitialContext().lookup("java:module/DriverFacade");
            driver = driverFacade.findById(id);
        } catch (NamingException ex) {
            Logger.getLogger(EmployeeConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return driver;
    }

}
