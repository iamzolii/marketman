/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

/**
 *
 * @author bzolt
 */
import facade.PartnerFacade;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import model.Partner;

@FacesConverter(value = "partnerConverter")
public class PartnerConverter implements Converter {
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return null;
        }
        Partner partner = (Partner) value;
        String idAsString = String.valueOf(partner.getId());
        return idAsString;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
                return null;
        }
        Integer id = Integer.valueOf(value);
        Partner partner = null;
        try {
            PartnerFacade partnerFacade = (PartnerFacade) new InitialContext().lookup("java:module/PartnerFacade");
            partner = partnerFacade.findById(id);
        } catch (NamingException ex) {
            Logger.getLogger(EmployeeConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return partner;
    }

}
