/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

/**
 *
 * @author bzolt
 */
import control.EmployeeController;
import facade.EmployeeFacade;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import model.Employee;

@FacesConverter(value = "employeeConverter")
public class EmployeeConverter implements Converter {
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return null;
        }
        Employee employee = (Employee) value;
        String idAsString = String.valueOf(employee.getId());
        return idAsString;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
                return null;
        }
        Integer id = Integer.valueOf(value);
        Employee employee = null;
        try {
            EmployeeFacade employeeFacade = (EmployeeFacade) new InitialContext().lookup("java:module/EmployeeFacade");
            employee = employeeFacade.findById(id);
        } catch (NamingException ex) {
            Logger.getLogger(EmployeeConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return employee;
    }

}
