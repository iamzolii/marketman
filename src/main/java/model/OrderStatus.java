/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author bzolt
 */
public enum OrderStatus {
        ACTIVE("Active"),
        STORNO("Storno"),
        DONE("Done");

        private final String stat;

        OrderStatus(String stat) {
            this.stat = stat;
        }

        public String getStatus() {
            return stat;
        }
}
