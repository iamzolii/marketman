/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author bzolt
 */
public enum ShippingStatus {
        ON_THE_WAY("On the way"),
        DONE("Done"),
        ACTIVE("Active");

        private final String stat;

        ShippingStatus(String stat) {
            this.stat = stat;
        }

        public String getStatus() {
            return stat;
        }
}
