/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author bzolt
 */
public enum ActiveStatus {
        ACTIVE("Active"),
        INACTIVE("Inactive");

        private final String act;

        ActiveStatus(String act) {
            this.act = act;
        }

        public String getAcitvity() {
            return act;
        }
        
}
