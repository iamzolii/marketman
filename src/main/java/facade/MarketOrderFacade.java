/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.MarketOrder;
import model.OrderStatus;

/**
 *
 * @author bzolt
 */
@Stateless
public class MarketOrderFacade extends AbstractFacade<MarketOrder> {

    @PersistenceContext(unitName = "com.elte_MarketMan_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MarketOrderFacade() {
        super(MarketOrder.class);
    }
    
    public List<MarketOrder> findAll() {
        Query q = em.createQuery("Select o from MarketOrder o ORDER BY o.created DESC");
        return q.getResultList();
    }
    
    public List<MarketOrder> findAllActive() {
        Query q = em.createQuery("Select o from MarketOrder o where o.status = :status ORDER BY o.created DESC").setParameter("status", OrderStatus.ACTIVE);
        return q.getResultList();
    }
    
    public List<MarketOrder> findAllActiveAndDone() {
        Query q = em.createQuery("Select o from MarketOrder o where o.status != :status ORDER BY o.created DESC").setParameter("status", OrderStatus.STORNO);
        return q.getResultList();
    }
    
    public MarketOrder findById(Integer id) {
        Query q = em.createQuery("Select o from MarketOrder o where o.id = :id").setParameter("id",id);
        return (MarketOrder) q.getSingleResult();
    }
    
    public MarketOrder findOrder(MarketOrder order) {
        Query q = em.createQuery("Select o from MarketOrder o where o.created = :created and o.name = :name and o.orderType = :orderType"
                + " and o.partner = :partner and o.status = :status and o.sumPrice = :sumPrice").setParameter("created", order.getCreated())
                .setParameter("name", order.getName())
                .setParameter("orderType", order.getOrderType()).setParameter("partner", order.getPartner()).setParameter("status", order.getStatus())
                .setParameter("sumPrice", order.getSumPrice());
        return (MarketOrder) q.getSingleResult();
    }
    
    public void flush() {
        em.flush();
    }
}
