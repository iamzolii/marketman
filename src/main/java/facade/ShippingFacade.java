/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Employee;
import model.Shipping;

/**
 *
 * @author bzolt
 */
@Stateless
public class ShippingFacade extends AbstractFacade<Shipping> {

    @PersistenceContext(unitName = "com.elte_MarketMan_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ShippingFacade() {
        super(Shipping.class);
    }
    
    public Shipping findById(Integer id) {
        Query q = em.createQuery("select s from Shipping s where s.id = :id").setParameter("id", id);
        return (Shipping) q.getSingleResult();
    }
    
    public void flush() {
        em.flush();
    }
    
    @Override
    public List<Shipping> findAll() {
        Query q = em.createQuery("select s from Shipping s ORDER BY s.shippingDate DESC");
        return q.getResultList();
    }
    
    public List<Shipping> findAllByEmployee(Employee employee) {
        Query q = em.createQuery("select s from Shipping s where s.driver.employee = :employee ORDER BY s.shippingDate DESC").setParameter("employee", employee);
        return q.getResultList();
    }
    
}
