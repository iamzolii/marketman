/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Item;

/**
 *
 * @author bzolt
 */
@Stateless
public class ItemFacade extends AbstractFacade<Item> {

    @PersistenceContext(unitName = "com.elte_MarketMan_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ItemFacade() {
        super(Item.class);
    }
    
    public Item findById(Integer id) {
        Query q = em.createQuery("select i from Item i where i.id = :id").setParameter("id", id);
        return (Item) q.getSingleResult();
    }
    
    @Override
    public List<Item> findAll() {
        Query q = em.createQuery("select i from Item i ORDER BY i.name");
        return q.getResultList();
    }
    
}
