/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import model.UserGroup;

/**
 *
 * @author bzolt
 */
@Stateless
public class UserGroupFacade extends AbstractFacade<UserGroup> {

    @PersistenceContext(unitName = "com.elte_MarketMan_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserGroupFacade() {
        super(UserGroup.class);
    }
    
     public UserGroup findByEmail(String email) {
        TypedQuery<UserGroup> query = em.createNamedQuery("findByEmail", UserGroup.class);
        query.setParameter("email", email);
        UserGroup group = null;
        try {
            group = query.getSingleResult();
        } catch (Exception e) {

        }
        return group;
    }
    
}
