/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import model.ActiveStatus;
import model.Employee;

/**
 *
 * @author bzolt
 */
@Stateless
public class EmployeeFacade extends AbstractFacade<Employee> {

    @PersistenceContext(unitName = "com.elte_MarketMan_war_1.0PU")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public EmployeeFacade() {
        super(Employee.class);
    }

    public Employee findById(Integer id) {
        Query q = em.createQuery("select e from Employee e where e.id = :id").setParameter("id", id);
        return (Employee) q.getSingleResult();
    }

    public Employee findByEmail(String email) {
        TypedQuery<Employee> query = em.createNamedQuery("findEmployeeByEmail", Employee.class);
        query.setParameter("email", email);
        Employee employee = null;
        try {
            employee = query.getSingleResult();
        } catch (Exception e) {

        }
        return employee;
    }

    public List<Employee> findAllActive() {
        Query q = em.createQuery("select e from Employee e where e.activeStatus = :status").setParameter("status", ActiveStatus.ACTIVE);
        return q.getResultList();
    }

    
    public void flush() {
        em.flush();
    }
}
