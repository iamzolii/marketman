/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Employee;
import model.Request;
import model.Task;

/**
 *
 * @author bzolt
 */
@Stateless
public class RequestFacade extends AbstractFacade<Request> {

    @PersistenceContext(unitName = "com.elte_MarketMan_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RequestFacade() {
        super(Request.class);
    }
    
    @Override
    public List<Request> findAll() {
       Query q = em.createQuery("select r from Request r ORDER BY r.requestDate DESC");
       return q.getResultList();
    }
    
    public List<Request> findAllByEmployee(Employee employee) {
       Query q = em.createQuery("select r from Request r where r.employee = :employee ORDER BY r.requestDate DESC").setParameter("employee", employee);
       return q.getResultList();
    }
    
    public Request findById(Integer id) {
       Query q = em.createQuery("select r from Request r where r.id = :id").setParameter("id", id);
       return (Request) q.getSingleResult();
    }
    
    public List<Request> findAllByTaskAndEmployee(Task task, Employee employee) {
        Query q = em.createQuery("select r from Request r where r.task = :task and r.employee = :employee ORDER BY r.requestDate DESC").setParameter("task", task).setParameter("employee", employee);
        return q.getResultList();
    }
    
    public List<Request> findAllByTask(Task task) {
        Query q = em.createQuery("select r from Request r where r.task = :task ORDER BY r.requestDate DESC").setParameter("task", task);
        return q.getResultList();
    }
    
    public List<Task> findAllTaskWithRequests() {
        Query q = em.createQuery("select r.task from Request r ");
        return q.getResultList();
    }
    
     public List<Employee> findAllEmployeeWithRequests() {
        Query q = em.createQuery("select distinct r.employee from Request r");
        return q.getResultList();
    }
    
}
