/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.ActiveStatus;
import model.Warehouse;

/**
 *
 * @author bzolt
 */
@Stateless
public class WarehouseFacade extends AbstractFacade<Warehouse> {

    @PersistenceContext(unitName = "com.elte_MarketMan_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WarehouseFacade() {
        super(Warehouse.class);
    }
    
    public Warehouse findById(Integer id) {
        Query q = em.createQuery("Select wh from Warehouse wh where wh.id = :id").setParameter("id", id);
        return (Warehouse) q.getSingleResult();
    }
    
    public List<Warehouse> findAllActive() {
        Query q = em.createQuery("Select wh from Warehouse wh where wh.activeStatus = :status").setParameter("status", ActiveStatus.ACTIVE);
        return q.getResultList();
    }
    
}
