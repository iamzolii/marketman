/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Car;

/**
 *
 * @author bzolt
 */
@Stateless
public class CarFacade extends AbstractFacade<Car> {

    @PersistenceContext(unitName = "com.elte_MarketMan_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CarFacade() {
        super(Car.class);
    }
    
    public Car findById(Integer id) {
        Query q = em.createQuery("select c from Car c where c.id = :id").setParameter("id", id);
        return (Car) q.getSingleResult();
    }
    
    public List<Car> findAllWorking() {
        Query q = em.createQuery("select c from Car c where c.condition = :condition").setParameter("condition", Car.Condition.WORKING);
        return q.getResultList();
    }
    
}
