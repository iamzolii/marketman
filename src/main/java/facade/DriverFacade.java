/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.ActiveStatus;
import model.Driver;
import model.Employee;

/**
 *
 * @author bzolt
 */
@Stateless
public class DriverFacade extends AbstractFacade<Driver> {

    @PersistenceContext(unitName = "com.elte_MarketMan_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DriverFacade() {
        super(Driver.class);
    }

    public Driver findById(Integer id) {
        Query q = em.createQuery("Select d from Driver d where d.id = :id").setParameter("id", id);
        return (Driver) q.getSingleResult();
    }

    public List<Driver> findAllActive() {
        Query q = em.createQuery("select d from Driver d where d.employee.activeStatus = :status and d.status = :status").setParameter("status", ActiveStatus.ACTIVE);
        return q.getResultList();
    }

    public Driver findByEmployeeId(Integer employeeId) {
        try {
            Query q = em.createQuery("select d from Driver d where d.employee.id = :employeeId").setParameter("employeeId", employeeId);
            return (Driver) q.getSingleResult();
        }
        catch(Exception ex) {
            return null;
        }
    }

}
