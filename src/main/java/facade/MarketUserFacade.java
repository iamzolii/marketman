/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import authentication.AuthenticationUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import model.Employee;
import model.MarketUser;
import model.UserGroup;

/**
 *
 * @author bzolt
 */
@Stateless
public class MarketUserFacade extends AbstractFacade<MarketUser> {

    @PersistenceContext(unitName = "com.elte_MarketMan_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MarketUserFacade() {
        super(MarketUser.class);
    }

    public void createUser(MarketUser user,String phoneNumber) {
        try {
            user.setPwd(AuthenticationUtils.encodeSHA256(user.getPwd()));
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        UserGroup group = new UserGroup(user.getEmail(),UserGroup.USERS_GROUP);
        Employee employee = new Employee(user.getName(),phoneNumber,user.getEmail());
        employee.setPosition(Employee.Position.NEW_EMPLOYEE);
        em.persist(user);
        em.persist(group);
        em.persist(employee);

    }

    public MarketUser findByEmail(String email) {
        TypedQuery<MarketUser> query = em.createNamedQuery("findUserByEmail", MarketUser.class);
        query.setParameter("email", email);
        MarketUser user = null;
        try {
            user = query.getSingleResult();
        } catch (Exception e) {

        }
        return user;
    }

}
