/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.ActiveStatus;
import model.Employee;
import model.Task;

/**
 *
 * @author bzolt
 */
@Stateless
public class TaskFacade extends AbstractFacade<Task> {

    @PersistenceContext(unitName = "com.elte_MarketMan_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TaskFacade() {
        super(Task.class);
    }
    
    public Task findById(Integer id) {
        Query q = em.createQuery("select t from Task t where t.id = :id").setParameter("id", id);
        return (Task) q.getSingleResult();
    }
    
    @Override
    public List<Task> findAll() {
        Query q = em.createQuery("select t from Task t ORDER BY t.taskDate DESC");
        return q.getResultList();
    }
    
    public List<Task> findAllActive() {
        Query q = em.createQuery("select t from Task t where t.activeStatus = :status ORDER BY t.taskDate DESC").setParameter("status", ActiveStatus.ACTIVE);
        return q.getResultList();
    }
    
    public List<Task> findAllActiveByEmail(String email) {
        Query q = em.createQuery("select t from Task t where t.employee.email = :email and t.activeStatus = :status ORDER BY t.taskDate DESC").setParameter("email", email).setParameter("status", ActiveStatus.ACTIVE);
        return q.getResultList();
    }
    
     public List<Task> findAllByEmail(String email) {
        Query q = em.createQuery("select t from Task t where t.employee.email = :email ORDER BY t.taskDate DESC").setParameter("email", email);
        return q.getResultList();
    }
    
    public List<Task> findAllByDate(Date date) {
        Query q = em.createQuery("select t from Task t where t.taskDate = :date ORDER BY t.taskDate DESC").setParameter("date", date);
        return q.getResultList();
    }
    
    public List<Task> findAllByDateAndEmployee(Date date, Employee employee) {
        Query q = em.createQuery("select t from Task t where t.employee = :employee and t.taskDate = :date ORDER BY t.taskDate DESC").setParameter("employee", employee).setParameter("date", date);
        return q.getResultList();
    }
    
    public List<Task> findAllByEmployee(Employee employee) { 
        Query q = em.createQuery("select t from Task t where t.employee = :employee ORDER BY t.taskDate DESC").setParameter("employee", employee);
        return q.getResultList();
    }
    
    public List<Task> findAllByDateAndEmail(Date date, String email) {
        Query q = em.createQuery("select t from Task t where t.taskDate = :date and t.employee.email = :email ORDER BY t.taskDate DESC").setParameter("date", date).setParameter("email", email);
        return q.getResultList();
    }
    
}
