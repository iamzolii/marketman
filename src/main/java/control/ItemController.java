/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import facade.ItemFacade;
import facade.WarehouseFacade;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import model.Item;
import model.Warehouse;

/**
 *
 * @author bzolt
 */
@Named(value = "itemController")
@ViewScoped
public class ItemController extends UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612735L;

    @EJB
    ItemFacade itemFacade;

    @EJB
    WarehouseFacade warehouseFacade;

    private String filteredName;
    private Warehouse filteredWarehouse;

    private List<Item> itemList;
    private List<Warehouse> warehouseList;
    private Item item;

    public ItemController() {
    }

    public void init() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            warehouseList = warehouseFacade.findAll();
            filter();
        }
    }

    public void initExistingItem() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            warehouseList = warehouseFacade.findAllActive();
            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();
            String itemId = params.get("item");
            item = itemFacade.findById(Integer.parseInt(itemId));
            if (item != null && !warehouseList.contains(item.getWarehouse())) {
                warehouseList.add(item.getWarehouse());
            }
        }
    }

    public void initNewItem() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            warehouseList = warehouseFacade.findAllActive();
            item = new Item();
        }
    }

    public void addNewItem() throws IOException {
        itemFacade.create(item);
        FacesContext.getCurrentInstance().getExternalContext().redirect("items.xhtml");
    }

    public void editItem() throws IOException {
        itemFacade.edit(item);
        FacesContext.getCurrentInstance().getExternalContext().redirect("view_item.xhtml?item=" + item.getId());
    }

    public void filter() {
        itemList = itemFacade.findAll();
        List<Item> tempItemList = new ArrayList<>();
        if (filteredName != null) {
            if (!filteredName.isEmpty()) {
                for (Item item : itemList) {
                    if (!item.getName().toUpperCase().contains(filteredName.toUpperCase())) {
                        tempItemList.add(item);
                    }
                }

            }
        }
        if (filteredWarehouse != null) {
            for (Item item : itemList) {
                if (!item.getWarehouse().equals(filteredWarehouse)) {
                    tempItemList.add(item);
                }
            }
        }
        itemList.removeAll(tempItemList);
    }

    public String getFilteredName() {
        return filteredName;
    }

    public void setFilteredName(String filteredName) {
        this.filteredName = filteredName;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public List<Warehouse> getWarehouseList() {
        return warehouseList;
    }

    public void setWarehouseList(List<Warehouse> warehouseList) {
        this.warehouseList = warehouseList;
    }

    public Warehouse getFilteredWarehouse() {
        return filteredWarehouse;
    }

    public void setFilteredWarehouse(Warehouse filteredWarehouse) {
        this.filteredWarehouse = filteredWarehouse;
    }

}
