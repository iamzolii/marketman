/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import authentication.AuthenticationUtils;
import facade.DriverFacade;
import facade.EmployeeFacade;
import facade.MarketUserFacade;
import facade.UserGroupFacade;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import model.ActiveStatus;
import model.Driver;
import model.Employee;
import model.Employee.Position;
import model.MarketUser;
import model.Request;
import model.UserGroup;

/**
 *
 * @author bzolt
 */
@Named(value = "employeeController")
@ViewScoped
public class EmployeeController extends UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612719L;

    @EJB
    private EmployeeFacade employeeFacade;
    @EJB
    private MarketUserFacade userFacade;
    @EJB
    private UserGroupFacade userGroupFacade;
    @EJB
    private DriverFacade driverFacade;

    private Employee employee;
    private List<Employee> employeeList;
    private Employee loggedInEmployee;
    private boolean adminCheckBox;
    private String newUserPwd;
    private MarketUser UserByEmployee;
    private UserGroup groupByEmployee;
    private String filteredName;
    private Position filteredPosition;
    private String filteredEmail;
    private Employee oldEmployee;

    private boolean showInactive;

    public EmployeeController() {
    }

    public void init() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            filter();
        }
    }

    public void initLoggedInEmployee() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            loggedInEmployee = employeeFacade.findByEmail(getLoggedInUser().getEmail());
        }
    }

    public ActiveStatus[] allActiveStatus() {
        return ActiveStatus.values();
    }

    public void initExistingEmployee() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();
            String employeeId = params.get("employee");
            oldEmployee = employeeFacade.findById(Integer.parseInt(employeeId));
            employee = employeeFacade.findById(Integer.parseInt(employeeId));
            UserByEmployee = userFacade.findByEmail(employee.getEmail());
            groupByEmployee = userGroupFacade.findByEmail(employee.getEmail());
            adminCheckBox = UserByEmployee.getUserRole().equals("admin");
        }
    }

    public void initNewEmployee() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            employee = new Employee();
        }
    }

    public void addNewEmployee() throws IOException {
        boolean emailAvailable = true;
        for (Employee tempEmp : employeeFacade.findAll()) {
            if (tempEmp.getEmail().equals(employee.getEmail())) {
                emailAvailable = false;
            }
        }
        if (emailAvailable) {
            employeeFacade.create(employee);
            createUserForEmployee();
            if (employee.getPosition().equals(Employee.Position.DRIVER)) {
                createDriverForEmployee();
            }
            FacesContext.getCurrentInstance().getExternalContext().redirect("employees.xhtml");
        } else {
            addErrorMessageForComponent("newEmployeeForm:email", "Email already in use!");
        }
    }

    public String activeStyleColor() {
        if (employee.getActiveStatus().equals(ActiveStatus.ACTIVE)) {
            return "green";
        }
        return "red";
    }

    public String activeStyleColorForUser() {
        if (loggedInEmployee.getActiveStatus().equals(ActiveStatus.ACTIVE)) {
            return "green";
        }
        return "red";
    }

    public boolean employeeStatusActive(Employee employee) {
        return employee.getActiveStatus().equals(ActiveStatus.ACTIVE);
    }

    public void createDriverForEmployee() {
        try {
            boolean existingEmp = employee.getId() != null;
            Context ic = new InitialContext();
            DataSource ds = (DataSource) ic.lookup("java:app/aramis");
            Connection conn = ds.getConnection();
            conn.setAutoCommit(false);
            conn.commit();
            conn.setAutoCommit(true);
            employeeFacade.flush();
            Driver driver = new Driver();
            Employee emp = null;
            if (existingEmp) {
                emp = employeeFacade.findById(employee.getId());
            } else {
                emp = employeeFacade.findById(employee.getId() + 1);
            }
            driver.setEmployee(emp);
            emp.setDriver(driver);
            driverFacade.create(driver);
            employeeFacade.edit(emp);
        } catch (Exception ex) {

        }
    }

    public void createUserForEmployee() {
        MarketUser user = new MarketUser();
        user.setEmail(employee.getEmail());
        user.setName(employee.getName());
        user.setUserRole("user");
        try {
            user.setPwd(AuthenticationUtils.encodeSHA256("new_employee"));
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        UserGroup userGroup = new UserGroup(user.getEmail(), UserGroup.USERS_GROUP);
        userFacade.create(user);
        userGroupFacade.create(userGroup);
    }

    public void filter() {
        if (!showInactive) {
            employeeList = employeeFacade.findAllActive();
        } else {
            employeeList = employeeFacade.findAll();
        }
        List<Employee> tempEmployeeList = new ArrayList<>();
        if (filteredName != null) {
            if (!filteredName.isEmpty()) {
                for (Employee employee : employeeList) {
                    if (!employee.getName().toUpperCase().contains(filteredName.toUpperCase())) {
                        tempEmployeeList.add(employee);
                    }
                }
            }
        }
        if (filteredPosition != null) {
            for (Employee employee : employeeList) {
                if (!employee.getPosition().equals(filteredPosition)) {
                    tempEmployeeList.add(employee);
                }
            }
        }
        if (filteredEmail != null) {
            if (!filteredEmail.isEmpty()) {
                for (Employee employee : employeeList) {
                    if (!employee.getEmail().contains(filteredEmail)) {
                        tempEmployeeList.add(employee);
                    }
                }
            }
        }
        employeeList.removeAll(tempEmployeeList);
    }

    public Position[] allPositions() {
        return Employee.Position.values();
    }

    public void editUserByEmployee() {
        UserByEmployee.setEmail(employee.getEmail());
        UserByEmployee.setName(employee.getName());
        groupByEmployee.setEmail(employee.getEmail());
        if (adminCheckBox == false) {
            UserByEmployee.setUserRole("user");
            groupByEmployee.setGroupName("user");
        } else {
            UserByEmployee.setUserRole("admin");
            groupByEmployee.setGroupName("admin");
        }
        if ((newUserPwd != null && !newUserPwd.isEmpty()) || employee.getActiveStatus().equals(ActiveStatus.INACTIVE)) {
            try {
                if (employee.getActiveStatus().equals(ActiveStatus.ACTIVE)) {
                    UserByEmployee.setPwd(AuthenticationUtils.encodeSHA256(newUserPwd));
                } else {
                    UserByEmployee.setPwd(AuthenticationUtils.encodeSHA256("blocked_user") + employee.hashCode());
                }
            } catch (Exception e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
        }
        userFacade.edit(UserByEmployee);
        userGroupFacade.edit(groupByEmployee);

    }

    public void editEmployee() throws IOException {

        boolean emailAvailable = true;
        if (!oldEmployee.getEmail().equals(employee.getEmail())) {
            for (Employee tempEmp : employeeFacade.findAll()) {
                if (tempEmp.getEmail().equals(employee.getEmail())) {
                    emailAvailable = false;
                }
            }
        }
        if (emailAvailable) {
            editUserByEmployee();
            if (employee.getPosition().equals(Employee.Position.DRIVER) && !oldEmployee.getPosition().equals(Employee.Position.DRIVER)) {
                if (driverFacade.findByEmployeeId(employee.getId()) == null) {
                    createDriverForEmployee();
                } else {
                    employee.getDriver().setStatus(ActiveStatus.ACTIVE);
                    driverFacade.edit(employee.getDriver());
                }
            } else if (employee.getPosition().equals(Employee.Position.DRIVER) && oldEmployee.getPosition().equals(Employee.Position.DRIVER)) {
                driverFacade.edit(employee.getDriver());
            } else if (!employee.getPosition().equals(Employee.Position.DRIVER) && oldEmployee.getPosition().equals(Employee.Position.DRIVER)) {
                employee.getDriver().setStatus(ActiveStatus.INACTIVE);
                driverFacade.edit(employee.getDriver());
            }
            employeeFacade.edit(employee);
            if (employee.getActiveStatus().equals(ActiveStatus.INACTIVE)) {
                deleteEmployee(employee);
            }
            FacesContext.getCurrentInstance().getExternalContext().redirect("view_employee.xhtml?employee=" + employee.getId());
        } else {
            addErrorMessageForComponent("employeeEditForm:email","Email already in use!");
        }
    }

    public void editUser() throws IOException {
        Employee emp = employeeFacade.findByEmail(employee.getEmail());
        if (emp != null && emp.equals(employee)) {
            editUserByEmployee();
            employeeFacade.edit(employee);
            FacesContext.getCurrentInstance().getExternalContext().redirect("user_data.xhtml");
        }
        addErrorMessageForComponent("employeeEditForm:email","Email already in use!");
    }

    public void deleteEmployee(Employee employeeToDelete) throws IOException {

        employeeToDelete.setActiveStatus(ActiveStatus.INACTIVE);
        if (employeeList != null && !employeeList.isEmpty() && employeeList.contains(employeeToDelete)) {
            employeeList.remove(employeeToDelete);
        }
        if (!employeeToDelete.getRequestList().isEmpty()) {
            List<Request> tempReqList = new ArrayList<>();
            for (Request request : employeeToDelete.getRequestList()) {
                if (request.getStatus().equals("Pending")) {
                    tempReqList.add(request);
                }
            }
            employeeToDelete.getRequestList().removeAll(tempReqList);
        }
        employeeFacade.edit(employeeToDelete);
        addMessage("Employee deactivated!");

    }

    public boolean activityRendered(Employee employee) {
        return employee.getActiveStatus().equals(ActiveStatus.INACTIVE);
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public Employee getLoggedInEmployee() {
        return loggedInEmployee;
    }

    public void setLoggedInEmployee(Employee loggedInEmployee) {
        this.loggedInEmployee = loggedInEmployee;
    }

    public boolean isAdminCheckBox() {
        return adminCheckBox;
    }

    public void setAdminCheckBox(boolean adminCheckBox) {
        this.adminCheckBox = adminCheckBox;
    }

    public String getNewUserPwd() {
        return newUserPwd;
    }

    public void setNewUserPwd(String newUserPwd) {
        this.newUserPwd = newUserPwd;
    }

    public String getFilteredName() {
        return filteredName;
    }

    public void setFilteredName(String filteredName) {
        this.filteredName = filteredName;
    }

    public Position getFilteredPosition() {
        return filteredPosition;
    }

    public void setFilteredPosition(Position filteredPosition) {
        this.filteredPosition = filteredPosition;
    }

    public String getFilteredEmail() {
        return filteredEmail;
    }

    public void setFilteredEmail(String filteredEmail) {
        this.filteredEmail = filteredEmail;
    }

    public boolean isShowInactive() {
        return showInactive;
    }

    public void setShowInactive(boolean showInactive) {
        this.showInactive = showInactive;
    }

}
