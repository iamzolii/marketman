/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import facade.CarFacade;
import facade.DriverFacade;
import facade.EmployeeFacade;
import facade.ItemFacade;
import facade.MarketOrderFacade;
import facade.ShippingFacade;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import model.ActiveStatus;
import model.Car;
import model.Driver;
import model.Employee;
import model.MarketOrder;
import model.OrderItem;
import model.OrderStatus;
import model.OrderType;
import model.Shipping;
import model.ShippingStatus;
import org.xhtmlrenderer.pdf.ITextRenderer;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

/**
 *
 * @author bzolt
 */
@Named(value = "shippingController")
@ViewScoped
public class ShippingController extends UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612793L;

    @EJB
    ShippingFacade shippingFacade;
    @EJB
    DriverFacade driverFacade;
    @EJB
    CarFacade carFacade;
    @EJB
    MarketOrderFacade orderFacade;
    @EJB
    EmployeeFacade employeeFacade;
    @EJB
    ItemFacade itemFacade;

    private List<Shipping> shippingList;
    private Shipping shipping;
    private Driver filteredDriver;
    private Date filteredShippingDate;
    private MarketOrder filteredOrder;
    private List<Driver> driverList;
    private List<Car> carList;
    private List<MarketOrder> orderList;
    private Set<MarketOrder> ordersToBeRemoved;

    public ShippingController() {
    }

    public void init() throws ParseException, IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            driverList = driverFacade.findAll();
            orderList = orderFacade.findAll();
            filter();
        }
    }

    public void initExistingShipping() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();
            String shippingId = params.get("shipping");
            if (shippingId == null) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("shippings.xhtml");
            } else if (shippingId.isEmpty()) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("shippings.xhtml");
            }
            shipping = shippingFacade.findById(Integer.parseInt(shippingId));
            carList = carFacade.findAllWorking();
            if (!carList.contains(shipping.getCar())) {
                carList.add(shipping.getCar());
            }
            orderList = orderFacade.findAllActive();
            driverList = driverFacade.findAllActive();
            if (!driverList.contains(shipping.getDriver())) {
                driverList.add(shipping.getDriver());
            }
            ordersToBeRemoved = new HashSet<>();
        }
    }

    public void shippingOnTheWay(Shipping shipping) {
        shipping.setStatus(ShippingStatus.ON_THE_WAY);
        shippingFacade.edit(shipping);
    }

    public boolean orderStatusActive(MarketOrder order) {
        return order.getStatus().equals(OrderStatus.ACTIVE);
    }

    public boolean shippingStatusActive(Shipping shipping) {
        return shipping.getStatus().equals(ShippingStatus.ACTIVE);
    }

    public boolean shippingStatusActiveOrOnTheWay(Shipping shipping) {
        return shipping.getStatus().equals(ShippingStatus.ACTIVE) || shipping.getStatus().equals(ShippingStatus.ON_THE_WAY);
    }

    public boolean shippingStatusDone(Shipping shipping) {
        return shipping.getStatus().equals(ShippingStatus.DONE);
    }

    public void initNewShipping() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            shipping = new Shipping();
            carList = carFacade.findAllWorking();
            driverList = driverFacade.findAllActive();
            orderList = orderFacade.findAllActive();
            shipping.getOrderList().add(new MarketOrder());
        }
    }

    public void addToOrderList() {
        shipping.getOrderList().add(new MarketOrder());
    }

    public void removeFromOrderList(int index) {
        MarketOrder order = shipping.getOrderList().get(index);
        shipping.getOrderList().remove(order);
    }

    public void addToExistingOrderList() {
        MarketOrder order = new MarketOrder();
        shipping.getOrderList().add(order);
    }

    public void removeFromExistingOrderList(int index) {
        MarketOrder order = shipping.getOrderList().get(index);
        shipping.getOrderList().remove(order);
        ordersToBeRemoved.add(order);
    }

    public boolean orderCancelled(MarketOrder order) {
        return order.getStatus().equals(OrderStatus.STORNO);
    }

    public void doneAll(Shipping shipping) throws ParseException {
        boolean allDone = true;
        for (MarketOrder so : shipping.getOrderList()) {
            if (so.getStatus().equals(OrderStatus.ACTIVE)) {
                allDone = false;
            }
        }
        if (allDone) {
            shipping.setStatus(ShippingStatus.DONE);
        } else {
            for (MarketOrder so : shipping.getOrderList()) {
                if (!so.getStatus().equals(OrderStatus.STORNO)) {
                    completeOrder(so);
                    orderFacade.edit(so);
                }
            }
            shipping.setStatus(ShippingStatus.DONE);
        }
        shippingFacade.edit(shipping);
    }

    public void doneOrder(MarketOrder order) throws ParseException {
        completeOrder(order);
    }

    public void completeOrder(MarketOrder order) throws ParseException {
        order.setStatus(OrderStatus.DONE);
        order.setCompleted(formattedDateInDate(new Date()));
        orderFacade.edit(order);
        if (order.getOrderType().equals(OrderType.SUPPLIER)) {
            for (OrderItem oi : order.getOrderItemList()) {
                oi.getItem().setQuantity(oi.getItem().getQuantity() + oi.getQuantity());
                itemFacade.edit(oi.getItem());
            }
        }
    }

    public void filter() throws ParseException {
        if (isUserInRole("admin")) {
            shippingList = shippingFacade.findAll();
        } else {
            Employee loggedInEmployee = employeeFacade.findByEmail(getLoggedInUser().getEmail());
            shippingList = shippingFacade.findAllByEmployee(loggedInEmployee);
        }
        List<Shipping> tempShippingList = new ArrayList<>();
        if (filteredDriver != null) {
            for (Shipping shipping : shippingList) {
                if (!shipping.getDriver().equals(filteredDriver)) {
                    tempShippingList.add(shipping);
                }
            }
        }
        if (filteredOrder != null) {
            boolean contains;
            for (Shipping shipping : shippingList) {
                contains = false;
                for (MarketOrder so : shipping.getOrderList()) {
                    if (so.equals(filteredOrder)) {
                        contains = true;
                    }
                }
                if (!contains) {
                    tempShippingList.add(shipping);
                }
            }
        }
        if (filteredShippingDate != null) {
            for (Shipping shipping : shippingList) {
                if (!formattedDateInDate(shipping.getShippingDate()).equals(formattedDateInDate(filteredShippingDate))) {
                    tempShippingList.add(shipping);
                }
            }
        }
        shippingList.removeAll(tempShippingList);
    }

    public void deleteShipping(Shipping shipping) throws IOException {
        List<MarketOrder> tempShippingOrderList = new ArrayList<>();
        for (MarketOrder so : shipping.getOrderList()) {
            tempShippingOrderList.add(so);
            so.getShippingList().remove(shipping);
            orderFacade.edit(so);
        }
        shipping.getOrderList().removeAll(tempShippingOrderList);
        shippingFacade.remove(shipping);
        if (shippingList != null && !shippingList.isEmpty() && !shippingList.contains(shipping)) {
            shippingList.remove(shipping);
            addMessage("Shipping deleted");
        } else {
            FacesContext.getCurrentInstance().getExternalContext().redirect("shippings.xhtml");
        }
    }

    public void editShipping() throws IOException {
        if (shipping.getOrderList() != null && !shipping.getOrderList().isEmpty()) {
            Set<MarketOrder> soSet = new HashSet<>(shipping.getOrderList());
            if (soSet.size() != shipping.getOrderList().size()) {
                addErrorMessage("Duplicate order on shipping");
            } else {

                for (MarketOrder order : shipping.getOrderList()) {
                    if (!order.getShippingList().contains(shipping)) {
                        order.getShippingList().add(shipping);
                        orderFacade.edit(order);
                    }
                }

                for (MarketOrder order : ordersToBeRemoved) {
                    if (!shipping.getOrderList().contains(order)) {
                        order.getShippingList().remove(shipping);
                        orderFacade.edit(order);
                    }
                }

                ordersToBeRemoved = new HashSet<>();

                shippingFacade.edit(shipping);

                FacesContext.getCurrentInstance().getExternalContext().redirect("view_shipping.xhtml?shipping=" + shipping.getId());
            }
        } else {
            addErrorMessage("You didn't select any order to be shipped");
        }
    }

    public void printShipping() throws ServletException {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        try {
            ec.setResponseContentType("application/pdf");
            String fileName = "shipping_" + shipping.getId() + ".pdf";
            ec.setResponseHeader("Content-Disposition", "inline; filename=" + fileName);

            HttpServletResponse response = (HttpServletResponse) ec.getResponse();
            ITextRenderer renderer = new ITextRenderer();
            Document pdf = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.getInstance(pdf, response.getOutputStream());

            if (!pdf.isOpen()) {
                pdf.open();
            }

            pdf.addTitle("Shipping_" + shipping.getId());
            pdf.addCreationDate();
            pdf.addCreator(getLoggedInUser().getName());
            pdf.addSubject("Shipping data");

            fillPdf(pdf);

            renderer.layout();
            response.reset();

            OutputStream outputStream = response.getOutputStream();
            renderer.createPDF(outputStream);
            outputStream.close();

            fc.responseComplete();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void fillPdf(Document pdf) throws DocumentException, IOException {
        BaseFont baseFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
        Font fontNormal = new Font(baseFont, 16, Font.NORMAL);
        Font fontBold = new Font(baseFont, 16, Font.BOLD);
        Font titleFont = new Font(baseFont, 32, Font.BOLD);

        Paragraph titleParagraph = new Paragraph("Shipping #" + shipping.getId(), titleFont);
        titleParagraph.setAlignment(Element.ALIGN_CENTER);
        titleParagraph.setSpacingAfter(24f);
        pdf.add(titleParagraph);

        PdfPTable table = new PdfPTable(2);
        table.addCell(new PdfPCell(new Paragraph("Driver", fontBold)));
        table.addCell(new PdfPCell(new Paragraph(shipping.getDriver().getEmployee().getName(), fontNormal)));
        table.addCell(new PdfPCell(new Paragraph("Car", fontBold)));
        table.addCell(new PdfPCell(new Paragraph(shipping.getCar().getName(), fontNormal)));
        table.addCell(new PdfPCell(new Paragraph("Shipping date", fontBold)));
        table.addCell(new PdfPCell(new Paragraph(formattedDateInString(shipping.getShippingDate()), fontNormal)));
        table.addCell(new PdfPCell(new Paragraph("Status", fontBold)));
        table.addCell(new PdfPCell(new Paragraph(shipping.getStatus().getStatus(), fontNormal)));

        PdfPTable table2 = new PdfPTable(1);
        PdfPCell ordersCell = new PdfPCell(new Paragraph("Orders", fontBold));
        ordersCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table2.addCell(ordersCell);

        PdfPTable table3 = new PdfPTable(5);
        table3.addCell(new PdfPCell(new Paragraph("Name", fontBold)));
        table3.addCell(new PdfPCell(new Paragraph("Address", fontBold)));
        table3.addCell(new PdfPCell(new Paragraph("Type", fontBold)));
        table3.addCell(new PdfPCell(new Paragraph("Sum", fontBold)));
        table3.addCell(new PdfPCell(new Paragraph("Status", fontBold)));

        PdfPTable table4 = new PdfPTable(5);
        for (MarketOrder so : shipping.getOrderList()) {
            table4.addCell(new PdfPCell(new Paragraph(so.getName(), fontNormal)));
            table4.addCell(new PdfPCell(new Paragraph(so.getPartner().getAddress(), fontNormal)));
            table4.addCell(new PdfPCell(new Paragraph(so.getOrderType().getType(), fontNormal)));
            table4.addCell(new PdfPCell(new Paragraph(so.getSumPrice().toString(), fontNormal)));
            table4.addCell(new PdfPCell(new Paragraph(so.getStatus().getStatus(), fontNormal)));
        }

        Font footerFont = new Font(baseFont, 14, Font.ITALIC);

        Paragraph createdBy = new Paragraph("Created by : " + getLoggedInUser().getName(), footerFont);
        Paragraph createdDate = new Paragraph("Date : " + formattedDateInString(new Date()), footerFont);
        createdBy.setSpacingBefore(12f);
        createdBy.setAlignment(Element.ALIGN_RIGHT);
        createdDate.setAlignment(Element.ALIGN_RIGHT);

        pdf.add(table);
        pdf.add(table2);
        pdf.add(table3);
        pdf.add(table4);

        pdf.add(createdBy);
        pdf.add(createdDate);

        pdf.close();
    }

    public boolean activityRenderedForEmployee(Employee employee) {
        return employee.getActiveStatus().equals(ActiveStatus.INACTIVE);
    }

    public boolean driverHaveLicenseForCar(Driver driver, Car car) {
        if (driver.getLicense() == null) {
            return false;
        }
        if (driver.getLicense().equals(Car.License.B) && car.getRequiredLicense().equals(Car.License.B)) {
            return true;
        } else if (driver.getLicense().equals(Car.License.C) && (car.getRequiredLicense().equals(Car.License.C) || car.getRequiredLicense().equals(Car.License.B))) {
            return true;
        } else if (driver.getLicense().equals(Car.License.D)) {
            return true;
        } else {
            return false;
        }
    }

    public void addNewShipping() throws NamingException, SQLException, IOException {
        if (shipping.getOrderList() != null && !shipping.getOrderList().isEmpty()) {
            Set<MarketOrder> soSet = new HashSet<>(shipping.getOrderList());
            if (soSet.size() != shipping.getOrderList().size()) {
                addErrorMessage("Duplicate order on shipping");
            } else {
                if (driverHaveLicenseForCar(shipping.getDriver(), shipping.getCar())) {
                    shipping.getDriver().getShippingList().add(shipping);
                    shippingFacade.create(shipping);
                    Context ic = new InitialContext();
                    DataSource ds = (DataSource) ic.lookup("java:app/aramis");
                    Connection conn = ds.getConnection();
                    conn.setAutoCommit(false);
                    conn.commit();
                    conn.setAutoCommit(true);
                    shippingFacade.flush();
                    Shipping newShipping = shippingFacade.findById(shipping.getId() + 1);
                    for (MarketOrder order : shipping.getOrderList()) {
                        order.getShippingList().add(newShipping);
                        orderFacade.edit(order);
                    }
                    FacesContext.getCurrentInstance().getExternalContext().redirect("shippings.xhtml");
                } else {
                    addErrorMessage("Driver doesn't have the required license for selected car ");
                }
            }
        } else {
            addErrorMessage("You didn't select any order to be shipped");
        }

    }

    public List<Shipping> getShippingList() {
        return shippingList;
    }

    public void setShippingList(List<Shipping> shippingList) {
        this.shippingList = shippingList;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public Driver getFilteredDriver() {
        return filteredDriver;
    }

    public void setFilteredDriver(Driver filteredDriver) {
        this.filteredDriver = filteredDriver;
    }

    public Date getFilteredShippingDate() {
        return filteredShippingDate;
    }

    public void setFilteredShippingDate(Date filteredShippingDate) {
        this.filteredShippingDate = filteredShippingDate;
    }

    public MarketOrder getFilteredOrder() {
        return filteredOrder;
    }

    public void setFilteredOrder(MarketOrder filteredOrder) {
        this.filteredOrder = filteredOrder;
    }

    public List<Driver> getDriverList() {
        return driverList;
    }

    public void setDriverList(List<Driver> driverList) {
        this.driverList = driverList;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }

    public List<MarketOrder> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<MarketOrder> orderList) {
        this.orderList = orderList;
    }

}
