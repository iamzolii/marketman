/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import facade.DriverFacade;
import facade.EmployeeFacade;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import model.ActiveStatus;
import model.Car;
import model.Driver;

/**
 *
 * @author bzolt
 */
@Named(value = "driverController")
@ViewScoped
public class DriverController extends UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612719L;

    @EJB
    DriverFacade driverFacade;
    @EJB
    EmployeeFacade employeeFacade;

    private List<Driver> driverList;

    private String filteredName;
    private Car.License filteredLicense;
    private Driver driver;

    private boolean showInactive;

    public DriverController() {
    }

    public void init() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            filter();
        }
    }

    public ActiveStatus[] allActiveStatus() {
        return ActiveStatus.values();
    }

    public void initExistingDriver() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();
            String driverId = params.get("driver");
            driver = driverFacade.findById(Integer.parseInt(driverId));
        }
    }

    public boolean driverStatusActive(Driver driver) {
        return driver.getEmployee().getActiveStatus().equals(ActiveStatus.ACTIVE);
    }
    
    public boolean ownStatusActive(Driver driver) {
        return driver.getStatus().equals(ActiveStatus.ACTIVE);
    }

    public Car.License[] allLicenses() {
        return Car.License.values();
    }

    public void filter() {
        if (!showInactive) {
            driverList = driverFacade.findAllActive();
        } else {
            driverList = driverFacade.findAll();
        }
        List<Driver> tempDriverList = new ArrayList<>();
        if (filteredName != null) {
            if (!filteredName.isEmpty()) {
                for (Driver driver : driverList) {
                    if (!driver.getEmployee().getName().toUpperCase().contains(filteredName.toUpperCase())) {
                        tempDriverList.add(driver);
                    }
                }
            }
        }
        if (filteredLicense != null) {
            for (Driver driver : driverList) {
                if (!driver.getLicense().equals(filteredLicense)) {
                    tempDriverList.add(driver);
                }
            }
        }
        driverList.removeAll(tempDriverList);
    }

    public void editDriver() throws IOException {
        if (driver.getEmployee().getActiveStatus().equals(ActiveStatus.INACTIVE)) {
            deleteDriver(driver);
        }
        driverFacade.edit(driver);
        employeeFacade.edit(driver.getEmployee());
        FacesContext.getCurrentInstance().getExternalContext().redirect("view_driver.xhtml?driver=" + driver.getId());
    }

    public void deleteDriver(Driver driver) throws IOException {
        if (driverList != null && !driverList.isEmpty() && driverList.contains(driver)) {
            driverList.remove(driver);
        }

        driver.getEmployee().setActiveStatus(ActiveStatus.INACTIVE);
        driverFacade.edit(driver);
        employeeFacade.edit(driver.getEmployee());
        addMessage("Delete successful");
    }

    public String activeStyleColor() {
        if (driver.getEmployee().getActiveStatus().equals(ActiveStatus.ACTIVE)) {
            return "green";
        }
        return "red";
    }

    public boolean activityRendered(Driver driver) {
        return driver.getEmployee().getActiveStatus().equals(ActiveStatus.INACTIVE);
    }
    
    public boolean activityRenderedForDriver(Driver driver) {
        return driver.getStatus().equals(ActiveStatus.INACTIVE);
    }

    public List<Driver> getDriverList() {
        return driverList;
    }

    public void setDriverList(List<Driver> driverList) {
        this.driverList = driverList;
    }

    public String getFilteredName() {
        return filteredName;
    }

    public void setFilteredName(String filteredName) {
        this.filteredName = filteredName;
    }

    public Car.License getFilteredLicense() {
        return filteredLicense;
    }

    public void setFilteredLicense(Car.License filteredLicense) {
        this.filteredLicense = filteredLicense;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public boolean isShowInactive() {
        return showInactive;
    }

    public void setShowInactive(boolean showInactive) {
        this.showInactive = showInactive;
    }

}
