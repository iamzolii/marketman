/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import facade.EmployeeFacade;
import facade.RequestFacade;
import facade.TaskFacade;
import java.io.IOException;
import javax.ejb.EJB;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import model.Task;
import javax.inject.Named;
import model.ActiveStatus;
import model.Employee;
import model.Request;

/**
 *
 * @author bzolt
 */
@Named(value = "taskController")
@ViewScoped
public class TaskController extends UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612724L;

    @EJB
    TaskFacade taskFacade;
    @EJB
    EmployeeFacade employeeFacade;
    @EJB
    RequestFacade requestFacade;

    private List<Task> taskList;
    private Task task;
    private List<Employee> employees;
    private Task oldTask;
    private List<Task> taskListForUser;
    private Employee filteredEmployee;
    private Date filteredDate;
    private String filteredName;
    private String filteredDone;
    private List<String> donePossibilities;

    private boolean showInactive;

    public TaskController() {
    }

    public void init() throws ParseException, IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            employees = employeeFacade.findAllActive();
            donePossibilities = new ArrayList<>();
            donePossibilities.add("Yes");
            donePossibilities.add("No");
            filter();
        }
    }

    public void initExistingTask() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            employees = employeeFacade.findAllActive();
            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();
            String taskId = params.get("task");
            task = taskFacade.findById(Integer.parseInt(taskId));
            if(!employees.contains(task.getEmployee())) {
                employees.add(task.getEmployee());
            }
            oldTask = task;
        }
    }

    public void initNewTask() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            task = new Task();
            employees = employeeFacade.findAllActive();
        }
    }

    public void deleteTask(Task task) throws IOException {
        if (taskList != null && !taskList.isEmpty() && taskList.contains(task)) {
            taskList.remove(task);
        }
        if (!task.getRequestList().isEmpty()) {
            List<Request> tempList = new ArrayList<>();

            for (Request request : task.getRequestList()) {
                if (request.getStatus().equals("Pending")) {
                    tempList.add(request);
                }
            }
            task.getRequestList().removeAll(tempList);
        }
        task.setActiveStatus(ActiveStatus.INACTIVE);
        taskFacade.edit(task);
        employeeFacade.edit(task.getEmployee());

        addMessage("Task deactivated!");
    }

    public boolean haveRequestsThatAreNotPending(Task task) {
        for (Request request : task.getRequestList()) {
            if (!request.getStatus().equals("Pending")) {
                return true;
            }
        }
        return false;
    }

    public boolean taskAlreadyHaveRequest(Task task) {
        for (Request request : requestFacade.findAllByTask(task)) {
            if (request.getEmployee().equals(task.getEmployee())) {
                return true;
            }
        }
        return false;
    }

    public void editTask() throws IOException, ParseException {
        if (task.getActiveStatus().equals(ActiveStatus.INACTIVE)) {
            deleteTask(task);
        }

        oldTask.getEmployee().getTaskList().remove(oldTask);
        employeeFacade.edit(oldTask.getEmployee());

        task.setTaskDate(formattedDateInDate(task.getTaskDate()));
        task.getEmployee().getTaskList().add(task);
        taskFacade.edit(task);
        employeeFacade.edit(task.getEmployee());

        FacesContext.getCurrentInstance().getExternalContext().redirect("view_task.xhtml?task=" + task.getId());
    }
    
    public boolean ownAdminTask(Task task) {
        Employee loggedInEmployee = employeeFacade.findByEmail(getLoggedInUser().getEmail());
        return task.getEmployee().equals(loggedInEmployee);
    }

    public void taskDone(Task task) throws ParseException {
        task.setDone("Yes");
        task.setCompletionDate(formattedDateInDate(new Date()));
        List<Request> tempList = new ArrayList<>();
        for (Request request : task.getRequestList()) {
            if (request.getStatus().equals("Pending")) {
                tempList.add(request);
            }
        }
        task.getRequestList().removeAll(tempList);
        taskFacade.edit(task);
    }

    public String activeStyleColor() {
        if (task.getActiveStatus().equals(ActiveStatus.ACTIVE)) {
            return "green";
        }
        return "red";
    }

    public void filter() throws ParseException {

        if (!showInactive) {
            taskList = taskFacade.findAllActive();
            taskListForUser = taskFacade.findAllActiveByEmail(getLoggedInUser().getEmail());
        } else {
            taskList = taskFacade.findAll();
            taskListForUser = taskFacade.findAllByEmail(getLoggedInUser().getEmail());
        }

        List<Task> tempTaskList = new ArrayList<>();
        List<Task> tempTaskListForUser = new ArrayList<>();

        if (filteredDate != null) {
            Date formattedDate = new Date();
            formattedDate = formattedDateInDate(filteredDate);
            for (Task task : taskList) {
                if (!task.getTaskDate().equals(formattedDate)) {
                    tempTaskList.add(task);
                }
            }
            for (Task task : taskListForUser) {
                if (!task.getTaskDate().equals(formattedDate)) {
                    tempTaskListForUser.add(task);
                }
            }
        }

        if (filteredEmployee != null) {
            for (Task task : taskList) {
                if (!task.getEmployee().equals(filteredEmployee)) {
                    tempTaskList.add(task);
                }
            }
        }

        if (filteredName != null) {
            if (!filteredName.isEmpty()) {
                for (Task task : taskList) {
                    if (!task.getName().toUpperCase().contains(filteredName.toUpperCase())) {
                        tempTaskList.add(task);
                    }
                }
                for (Task task : taskListForUser) {
                    if (!task.getName().contains(filteredName)) {
                        tempTaskListForUser.add(task);
                    }
                }
            }
        }

        if (filteredDone != null) {
            if (!filteredDone.isEmpty()) {

                for (Task task : taskList) {
                    if (!task.getDone().equals(filteredDone)) {
                        tempTaskList.add(task);
                    }
                }
                for (Task task : taskListForUser) {
                    if (!task.getDone().equals(filteredDone)) {
                        tempTaskListForUser.add(task);
                    }
                }

            }
        }
        taskList.removeAll(tempTaskList);
        taskListForUser.removeAll(tempTaskListForUser);
    }

    public void addNewTask() throws IOException, ParseException {
        task.setDone("No");
        task.getEmployee().getTaskList().add(task);
        task.setTaskDate(formattedDateInDate(task.getTaskDate()));
        taskFacade.create(task);
        employeeFacade.edit(task.getEmployee());
        FacesContext.getCurrentInstance().getExternalContext().redirect("tasks.xhtml");
    }

    public boolean taskStatusActive(Task task) {
        return task.getActiveStatus().equals(ActiveStatus.ACTIVE);
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public EmployeeFacade getEmployeeFacade() {
        return employeeFacade;
    }

    public void taskUndone(Task task) {
        task.setDone("No");
        task.setCompletionDate(null);
        taskFacade.edit(task);
    }

    public ActiveStatus[] allActiveStatus() {
        return ActiveStatus.values();
    }

    public boolean activityRenderedForTask(Task task) {
        return task.getActiveStatus().equals(ActiveStatus.INACTIVE);
    }

    public boolean activityRendered(Task task) {
        return task.getEmployee().getActiveStatus().equals(ActiveStatus.INACTIVE);
    }

    public List<Task> getTaskListForUser() {
        return taskListForUser;
    }

    public void setTaskListForUser(List<Task> taskListForUser) {
        this.taskListForUser = taskListForUser;
    }

    public Employee getFilteredEmployee() {
        return filteredEmployee;
    }

    public void setFilteredEmployee(Employee filteredEmployee) {
        this.filteredEmployee = filteredEmployee;
    }

    public Date getFilteredDate() {
        return filteredDate;
    }

    public void setFilteredDate(Date filteredDate) {
        this.filteredDate = filteredDate;
    }

    public String getFilteredName() {
        return filteredName;
    }

    public void setFilteredName(String filteredName) {
        this.filteredName = filteredName;
    }

    public String getFilteredDone() {
        return filteredDone;
    }

    public void setFilteredDone(String filteredDone) {
        this.filteredDone = filteredDone;
    }

    public List<String> getDonePossibilities() {
        return donePossibilities;
    }

    public void setDonePossibilities(List<String> donePossibilities) {
        this.donePossibilities = donePossibilities;
    }

    public boolean isShowInactive() {
        return showInactive;
    }

    public void setShowInactive(boolean showInactive) {
        this.showInactive = showInactive;
    }

}
