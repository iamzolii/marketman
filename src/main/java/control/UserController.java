/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import model.MarketUser;

/**
 *
 * @author bzolt
 */
@Named(value = "userController")
@SessionScoped
public class UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612780L;

    private static MarketUser loggedInUser;

    public UserController() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        loggedInUser = (MarketUser) sessionMap.get("User");
    }

    public MarketUser getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(MarketUser loggedInUser) {
        UserController.loggedInUser = loggedInUser;
    }
    
    public boolean isUserInRole(String role) {
        return loggedInUser != null && loggedInUser.getUserRole().equals(role);
    }

    public void alreadyLoggedIn() throws IOException {
        if (loggedInUser != null && loggedInUser.getId() != null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("user_data.xhtml");
        }
    }

    public void addErrorMessage(String msg) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, message);
    }

    public void addMessage(String msg) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(msg));
    }

    public void addErrorMessageForComponent(String componentId, String msg) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(componentId, message);
    }

    public String formattedDateInString(Date date) {
        if (date != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
            return formatter.format(date);
        }
        return "";
    }

    public Date formattedDateInDate(Date date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
        String dateToFormat = formatter.format(date);
        return formatter.parse(dateToFormat);
    }

}
