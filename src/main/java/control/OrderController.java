/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import facade.ItemFacade;
import facade.MarketOrderFacade;
import facade.PartnerFacade;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import model.ActiveStatus;
import model.Item;
import model.MarketOrder;
import model.OrderItem;
import model.OrderStatus;
import model.OrderType;
import model.Partner;

/**
 *
 * @author bzolt
 */
@Named(value = "orderController")
@ViewScoped
public class OrderController extends UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612742L;

    @EJB
    MarketOrderFacade orderFacade;
    @EJB
    PartnerFacade partnerFacade;
    @EJB
    ItemFacade itemFacade;

    private List<MarketOrder> orderList;
    private List<Partner> partnerList;
    private List<Item> itemList;
    private MarketOrder order;
    private Partner filteredPartner;
    private Date filteredCreated;
    private Date filteredCompleted;
    private boolean showStorno;
    private Date filteredDue;

    private List<OrderItem> orderItemList;

    public OrderController() {
    }

    public void init() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            itemList = itemFacade.findAll();
            partnerList = partnerFacade.findAll();
            filter();
        }
    }

    public void initNewOrder() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            itemList = itemFacade.findAll();
            partnerList = partnerFacade.findAllActive();
            order = new MarketOrder();
            orderItemList = new ArrayList<>();
            OrderItem oi = new OrderItem();
            orderItemList.add(oi);
        }
    }

    public void initExistingOrder() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            itemList = itemFacade.findAll();
            partnerList = partnerFacade.findAll();
            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();
            String orderId = params.get("order");
            order = orderFacade.findById(Integer.parseInt(orderId));
            orderItemList = order.getOrderItemList();
        }
    }

    public void completeOrder(MarketOrder order) throws ParseException {
        order.setStatus(OrderStatus.DONE);
        order.setCompleted(formattedDateInDate(new Date()));
        orderFacade.edit(order);
        if (order.getOrderType().equals(OrderType.SUPPLIER)) {
            for (OrderItem oi : order.getOrderItemList()) {
                oi.getItem().setQuantity(oi.getItem().getQuantity() + oi.getQuantity());
                itemFacade.edit(oi.getItem());
            }
        }
        addMessage("Order completed");
    }

    public boolean orderActive(MarketOrder order) {
        return order.getStatus().equals(OrderStatus.ACTIVE);
    }

    public void addToItemList() {
        OrderItem oi = new OrderItem();
        orderItemList.add(oi);
    }

    public void removeFromItemList(int index) {
        OrderItem oi = orderItemList.get(index);
        orderItemList.remove(oi);
    }

    public String styleColor(MarketOrder order) {
        if (order.getStatus().equals(OrderStatus.STORNO)) {
            return "red";
        } else {
            return "green";
        }
    }

    public boolean activityRenderedForPartner(Partner partner) {
        return partner.getActiveStatus().equals(ActiveStatus.INACTIVE);
    }

    public OrderType[] allOrderTypes() {
        return OrderType.values();
    }

    public OrderStatus[] allOrderStatus() {
        return OrderStatus.values();
    }

    public void filter() {
        if (showStorno) {
            orderList = orderFacade.findAll();
        } else {
            orderList = orderFacade.findAllActiveAndDone();
        }
        List<MarketOrder> tempOrderList = new ArrayList<>();
        if (filteredCompleted != null) {
            for (MarketOrder order : orderList) {
                if (!formattedDateInString(order.getCompleted()).equals(formattedDateInString(filteredCompleted))) {
                    tempOrderList.add(order);
                }
            }
        }
        if (filteredDue != null) {
            for (MarketOrder order : orderList) {
                if (!formattedDateInString(order.getDue()).equals(formattedDateInString(filteredDue))) {
                    tempOrderList.add(order);
                }
            }
        }
        if (filteredCreated != null) {
            for (MarketOrder order : orderList) {
                if (!formattedDateInString(order.getCreated()).equals(formattedDateInString(filteredCreated))) {
                    tempOrderList.add(order);
                }
            }
        }
        if (filteredPartner != null) {
            for (MarketOrder order : orderList) {
                if (!order.getPartner().equals(filteredPartner)) {
                    tempOrderList.remove(order);
                }
            }
        }
        orderList.removeAll(tempOrderList);
    }

    public void addNewOrder() throws ParseException, IOException, NamingException, SQLException {
        boolean emptyOrder = false;
        boolean zeroQuantity = false;
        if (orderItemList.isEmpty()) {
            addErrorMessage("Order has no items!");
            emptyOrder = true;
        }
        if (!emptyOrder) {
            for (OrderItem oi : orderItemList) {
                if (oi.getQuantity() == 0) {
                    zeroQuantity = true;
                    addErrorMessage("You can't add 0 of " + oi.getItem().getName());
                    break;
                }
            }
            if (!zeroQuantity) {
                Set<OrderItem> oiSet = new HashSet<>(orderItemList);
                if (oiSet.size() != orderItemList.size()) {
                    addErrorMessage("Duplicate item on order!");
                } else {
                    order.setCreated(formattedDateInDate(new Date()));
                    orderFacade.create(order);
                    Context ic = new InitialContext();
                    DataSource ds = (DataSource) ic.lookup("java:app/aramis");
                    Connection conn = ds.getConnection();
                    conn.setAutoCommit(false);
                    conn.commit();
                    conn.setAutoCommit(true);
                    orderFacade.flush();
                    MarketOrder newOrder = orderFacade.findOrder(order);
                    Item errorItem = new Item();
                    for (OrderItem oi : orderItemList) {
                        oi.setOrder(newOrder);
                        newOrder.getOrderItemList().add(oi);
                        int newQcs = oi.getItem().getQuantity() - oi.getQuantity();
                        if (newQcs < 0 && newOrder.getOrderType().equals(OrderType.CUSTOMER)) {
                            orderFacade.remove(newOrder);
                            newOrder = null;
                            errorItem = oi.getItem();
                            break;
                        }
                        if (newOrder.getOrderType().equals(OrderType.CUSTOMER)) {
                            oi.getItem().setQuantity(newQcs);
                        }
                        newOrder.setSumPrice(newOrder.getSumPrice() + (oi.getItem().getUnitPrice() * oi.getQuantity()));
                    }
                    if (newOrder != null) {
                        orderFacade.edit(newOrder);
                        for (OrderItem oi : orderItemList) {
                            itemFacade.edit(oi.getItem());
                        }
                        FacesContext.getCurrentInstance().getExternalContext().redirect("orders.xhtml");
                    } else {
                        addErrorMessage("There are only " + errorItem.getQuantity() + " pcs of " + errorItem.getName() + " on stock");
                    }
                }
            }
        }

    }

    public void deleteOrder(MarketOrder order) {
        order.setStatus(OrderStatus.STORNO);
        if (order.getOrderType().equals(OrderType.CUSTOMER)) {
            for (OrderItem oi : order.getOrderItemList()) {
                oi.getItem().setQuantity(oi.getItem().getQuantity() + oi.getQuantity());
                itemFacade.edit(oi.getItem());
            }
        }
        orderFacade.edit(order);
        addMessage("Order cancelled");
    }

    public List<MarketOrder> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<MarketOrder> orderList) {
        this.orderList = orderList;
    }

    public MarketOrder getOrder() {
        return order;
    }

    public void setOrder(MarketOrder order) {
        this.order = order;
    }

    public List<Partner> getPartnerList() {
        return partnerList;
    }

    public void setPartnerList(List<Partner> partnerList) {
        this.partnerList = partnerList;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public Partner getFilteredPartner() {
        return filteredPartner;
    }

    public void setFilteredPartner(Partner filteredPartner) {
        this.filteredPartner = filteredPartner;
    }

    public Date getFilteredCreated() {
        return filteredCreated;
    }

    public void setFilteredCreated(Date filteredCreated) {
        this.filteredCreated = filteredCreated;
    }

    public Date getFilteredCompleted() {
        return filteredCompleted;
    }

    public void setFilteredCompleted(Date filteredCompleted) {
        this.filteredCompleted = filteredCompleted;
    }

    public boolean isShowStorno() {
        return showStorno;
    }

    public void setShowStorno(boolean showStorno) {
        this.showStorno = showStorno;
    }

    public String firstItemOfOrder(MarketOrder order) {
        return order.getOrderItemList().get(0).getItem().getName();
    }

    public List<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public Date getFilteredDue() {
        return filteredDue;
    }

    public void setFilteredDue(Date filteredDue) {
        this.filteredDue = filteredDue;
    }

    public void setOrderFacade(MarketOrderFacade orderFacade) {
        this.orderFacade = orderFacade;
    }
    
    public void setItemFacade(ItemFacade itemFacade) {
        this.itemFacade = itemFacade;
    }
}
