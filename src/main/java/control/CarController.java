/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import facade.CarFacade;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import model.Car;

/**
 *
 * @author bzolt
 */
@Named(value = "carController")
@ViewScoped
public class CarController extends UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612771L;

    @EJB
    CarFacade carFacade;

    private String filteredName;
    private String filteredPlate;
    private Car.Condition filteredCondition;
    private Car car;

    private List<Car> carList;

    public CarController() {
    }

    public void init() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            filter();
        }
    }

    public void initExistingCar() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();
            String carId = params.get("car");
            car = carFacade.findById(Integer.parseInt(carId));
        }
    }

public void initNewCar() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            car = new Car();
        }
    }

    public void filter() {
        carList = carFacade.findAll();
        List<Car> tempCarList = new ArrayList<>();
        if (filteredName != null) {
            if (!filteredName.isEmpty()) {
                for (Car car : carList) {
                    if (!car.getName().toUpperCase().contains(filteredName.toUpperCase())) {
                        tempCarList.add(car);
                    }
                }
            }
        }
        if (filteredPlate != null) {
            if (!filteredPlate.isEmpty()) {
                filteredPlate = filteredPlate.toUpperCase();
                for (Car car : carList) {
                    if (!car.getPlateNumber().contains(filteredPlate)) {
                        tempCarList.add(car);
                    }
                }
            }
        }
        if (filteredCondition != null) {
            for (Car car : carList) {
                if (!car.getCondition().equals(filteredCondition)) {
                    tempCarList.add(car);
                }
            }
        }
        carList.removeAll(tempCarList);
    }

    public void editCar() throws IOException {
        carFacade.edit(car);
        FacesContext.getCurrentInstance().getExternalContext().redirect("view_car.xhtml?car=" + car.getId());
    }

    public void newCar() throws IOException {
        carFacade.create(car);
        FacesContext.getCurrentInstance().getExternalContext().redirect("cars.xhtml");
    }

    public Car.Condition[] allConditions() {
        return Car.Condition.values();
    }

    public Car.CarType[] allTypes() {
        return Car.CarType.values();
    }

    public Car.License[] allLicenses() {
        return Car.License.values();
    }

    public String getFilteredName() {
        return filteredName;
    }

    public void setFilteredName(String filteredName) {
        this.filteredName = filteredName;
    }

    public String getFilteredPlate() {
        return filteredPlate;
    }

    public void setFilteredPlate(String filteredPlate) {
        this.filteredPlate = filteredPlate;
    }

    public Car.Condition getFilteredCondition() {
        return filteredCondition;
    }

    public void setFilteredCondition(Car.Condition filteredCondition) {
        this.filteredCondition = filteredCondition;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

}
