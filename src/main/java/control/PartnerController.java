/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import facade.PartnerFacade;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import model.ActiveStatus;
import model.Partner;
import validator.EmailValidator;

/**
 *
 * @author bzolt
 */
@Named(value = "partnerController")
@ViewScoped
public class PartnerController extends UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612713L;

    @EJB
    PartnerFacade partnerFacade;

    Partner partner;

    private List<Partner> partnerList;
    private Set<String> partnerTypes;

    private String filteredName;
    private String filteredEmail;
    private String filteredType;

    private boolean showInactive;

    public PartnerController() {
    }

    public void init() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            initPartnerTypes();
            filter();
        }
    }

    public List<Partner> getPartnerList() {
        return partnerList;
    }

    public String activeStyleColor() {
        if (partner.getActiveStatus().equals(ActiveStatus.ACTIVE)) {
            return "green";
        }
        return "red";
    }

    public void initNewPartner() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            partner = new Partner();
            partnerTypes = new HashSet<>();
            initPartnerTypes();
        }
    }

    public void initExistingPartner() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();
            String partnerId = params.get("partner");
            partner = partnerFacade.findById(Integer.parseInt(partnerId));
            initPartnerTypes();
        }
    }

    public void filter() {
        if (!showInactive) {
            partnerList = partnerFacade.findAllActive();
        } else {
            partnerList = partnerFacade.findAll();
        }
        List<Partner> tempPartnerList = new ArrayList<>();
        if (filteredName != null) {
            if (!filteredName.isEmpty()) {
                for (Partner partner : partnerList) {
                    if (!partner.getName().toUpperCase().contains(filteredName.toUpperCase())) {
                        tempPartnerList.add(partner);
                    }
                }
            }
        }
        if (filteredEmail != null) {
            if (!filteredEmail.isEmpty()) {
                for (Partner partner : partnerList) {
                    if (partner.getEmail() == null) {
                        tempPartnerList.add(partner);
                    } else if (!partner.getEmail().contains(filteredEmail)) {
                        tempPartnerList.add(partner);
                    }
                }
            }
        }
        if (filteredType != null) {
            if (!filteredType.isEmpty()) {
                for (Partner partner : partnerList) {
                    if (!partner.getType().equals(filteredType)) {
                        tempPartnerList.add(partner);
                    }
                }
            }
        }
        partnerList.removeAll(tempPartnerList);
    }

    public void permanentDelete(Partner partner) throws IOException {
        if (partner.getMarketOrderList().isEmpty()) {
            partnerFacade.remove(partner);
            if (partnerList != null && !partnerList.isEmpty() && partnerList.contains(partner)) {
                partnerList.remove(partner);
                addMessage("Delete successful");
            }
            else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("partners.xhtml");
            }
        } else {
            addErrorMessage("Partner can't be deleted because he already has orders");
        }
    }

    public boolean activityRendered(Partner partner) {
        return partner.getActiveStatus().equals(ActiveStatus.INACTIVE);
    }

    public void initPartnerTypes() {
        partnerTypes = new HashSet<>();
        partnerTypes.add("Customer");
        partnerTypes.add("Supplier");
    }

    public boolean validatePartnerEmail(String email) {
        EmailValidator emailValidator = new EmailValidator();
        return emailValidator.validateString(email);
    }

    public boolean partnerStatusActive(Partner partner) {
        return partner.getActiveStatus().equals(ActiveStatus.ACTIVE);
    }

    public Partner getPartner() {
        return partner;
    }

    public ActiveStatus[] allActiveStatus() {
        return ActiveStatus.values();
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public Set<String> getPartnerTypes() {
        return partnerTypes;
    }

    public void setPartnerTypes(Set<String> partnerTypes) {
        this.partnerTypes = partnerTypes;
    }

    public void addNewPartner() throws IOException {
        if (validatePartnerEmail(partner.getEmail())) {
            partnerFacade.create(partner);
            FacesContext.getCurrentInstance().getExternalContext().redirect("partners.xhtml");
        } else {
            addErrorMessageForComponent("parntnerEditForm:email","Bad format for Email");
        }
    }

    public void editPartner() throws IOException {
        if (validatePartnerEmail(partner.getEmail())) {
            partnerFacade.edit(partner);
            FacesContext.getCurrentInstance().getExternalContext().redirect("view_partner.xhtml?partner=" + partner.getId());
        } else {
            addErrorMessageForComponent("parntnerEditForm:email","Bad format for Email");
        }
    }

    public void deletePartner(Partner partner) {
        if (partnerList != null && !partnerList.isEmpty() && partnerList.contains(partner)) {
            partnerList.remove(partner);
        }
        partner.setActiveStatus(ActiveStatus.INACTIVE);
        partnerFacade.edit(partner);
        addMessage("Partner deactivated!");
    }

    public String getFilteredName() {
        return filteredName;
    }

    public void setFilteredName(String filteredName) {
        this.filteredName = filteredName;
    }

    public String getFilteredEmail() {
        return filteredEmail;
    }

    public void setFilteredEmail(String filteredEmail) {
        this.filteredEmail = filteredEmail;
    }

    public String getFilteredType() {
        return filteredType;
    }

    public void setFilteredType(String filteredType) {
        this.filteredType = filteredType;
    }

    public boolean isShowInactive() {
        return showInactive;
    }

    public void setShowInactive(boolean showInactive) {
        this.showInactive = showInactive;
    }

}
