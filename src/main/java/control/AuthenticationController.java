package control;

import facade.MarketUserFacade;
import java.io.IOException;
import java.io.Serializable;
import java.security.Principal;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import model.MarketUser;

@Named(value = "authenticationController")
@SessionScoped
public class AuthenticationController extends UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612778L;
    private static Logger log = Logger.getLogger(AuthenticationController.class.getName());

    @EJB
    private MarketUserFacade userFacade;

    private MarketUser user;
    private MarketUser loginUser;
    private String confirmPassword;
    private String phoneNumber;

    private String script;

    public AuthenticationController() {
    }

    public void init() throws IOException {
        alreadyLoggedIn();
        script = "document.getElementById('loginForm:validatorMessage').style.display = 'none'";
        FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("loginForm:script");
        user = new MarketUser();
        loginUser = null;
    }

    public void register() throws IOException {
        user.setUserRole("user");
        boolean alreadyInUse = false;
        for (MarketUser userIt : userFacade.findAll()) {
            if (userIt.getEmail().equals(user.getEmail())) {
                alreadyInUse = true;
            }
        }
        if (!alreadyInUse) {
            try {
                userFacade.createUser(user, phoneNumber);
                log.info("New user created");
                FacesContext.getCurrentInstance().getExternalContext().redirect("registration_successful.xhtml");
            } catch (Exception ex) {

            }
        } else {
            addErrorMessageForComponent("registrationForm:email","Email already in use!");
        }

    }

    public void login() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.login(user.getEmail(), user.getPwd());
        } catch (ServletException e) {
            script = "document.getElementById('loginForm:validatorMessage').style.display = 'flex'";
            FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("loginForm:script");
            return;
        }
        Principal principal = request.getUserPrincipal();
        loginUser = userFacade.findByEmail(principal.getName());
        log.info("Authentication done for user: " + principal.getName());
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        sessionMap.put("User", loginUser);
        FacesContext.getCurrentInstance().getExternalContext().redirect("user_data.xhtml");
    }

    public void logout() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            loginUser = null;
            request.logout();
            // clear the session
            ((HttpSession) context.getExternalContext().getSession(false)).invalidate();
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            Map<String, Object> sessionMap = externalContext.getSessionMap();
            sessionMap.remove("User");
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } catch (ServletException e) {
            log.log(Level.SEVERE, "Failed to logout user!", e);
        }
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public MarketUser getUser() {
        return user;
    }

    public void setUser(MarketUser user) {
        this.user = user;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public MarketUser getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(MarketUser loginUser) {
        this.loginUser = loginUser;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
