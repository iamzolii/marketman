/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import facade.EmployeeFacade;
import facade.RequestFacade;
import facade.TaskFacade;
import java.io.IOException;
import javax.ejb.EJB;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import model.ActiveStatus;
import model.Employee;
import model.Request;
import model.Task;

/**
 *
 * @author bzolt
 */
@Named(value = "requestController")
@ViewScoped
public class RequestController extends UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612726L;

    @EJB
    RequestFacade requestFacade;
    @EJB
    EmployeeFacade employeeFacade;
    @EJB
    TaskFacade taskFacade;

    private List<Request> requestList;
    private List<Request> requestListForUser;
    private Request request;
    private Task taskForNewRequest;

    private Task filteredTask;
    private Date filteredDate;
    private Employee filteredEmployee;
    private Employee currentEmployee;
    private String filteredStatus;

    private List<Task> tasks;
    private List<Employee> employees;
    private List<String> statusList;

    public RequestController() {
    }

    public void init() throws ParseException, IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            statusList = new ArrayList<>();
            statusList.add("Pending");
            statusList.add("Declined");
            statusList.add("Accepted");

            currentEmployee = employeeFacade.findByEmail(getLoggedInUser().getEmail());
            if (isUserInRole("admin")) {
                tasks = requestFacade.findAllTaskWithRequests();
                employees = requestFacade.findAllEmployeeWithRequests();
            } else {
                tasks = taskFacade.findAllByEmployee(currentEmployee);
            }
            filter();
        }
    }

    public void initExistingRequest() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();
            String requestId = params.get("req");
            request = requestFacade.findById(Integer.parseInt(requestId));
        }
    }

    public void initNewRequest() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();
            String taskId = params.get("task");
            taskForNewRequest = taskFacade.findById(Integer.parseInt(taskId));
            request = new Request();
        }
    }

    public void newRequest() throws IOException, ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
        String formattedDate = formatter.format(new Date());
        request.setRequestDate(formatter.parse(formattedDate));
        request.setTask(taskForNewRequest);
        taskForNewRequest.getRequestList().add(request);
        request.setEmployee(taskForNewRequest.getEmployee());
        request.setStatus("Pending");
        request.getEmployee().getRequestList().add(request);
        requestFacade.create(request);
        taskFacade.edit(taskForNewRequest);
        employeeFacade.edit(request.getEmployee());
        FacesContext.getCurrentInstance().getExternalContext().redirect("requests.xhtml");
    }

    public boolean activityRendered(Request request) {
        return request.getEmployee().getActiveStatus().equals(ActiveStatus.INACTIVE);
    }

    public boolean activityRenderedForTask(Request request) {
        return request.getTask().getActiveStatus().equals(ActiveStatus.INACTIVE);
    }

    public String formattedDateInString(Date date) {
        if (date != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
            return formatter.format(date);
        }
        return "";
    }

    public Date formattedDateInDate(Date date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
        String dateToFormat = formatter.format(date);
        return formatter.parse(dateToFormat);
    }

    public void acceptRequest(Request request) throws IOException {
        request.setStatus("Accepted");
        requestFacade.edit(request);
        FacesContext.getCurrentInstance().getExternalContext().redirect("edit_task.xhtml?task=" + request.getTask().getId());
    }

    public void declineRequest(Request request) {
        request.setStatus("Declined");
        requestFacade.edit(request);
        addMessage("Request declined!");
    }

    public void deleteRequest(Request request) {
        requestListForUser.remove(request);
        requestFacade.remove(request);
        addMessage("Request deleted!");
    }

    public void editRequestReason() throws IOException {
        requestFacade.edit(request);
        FacesContext.getCurrentInstance().getExternalContext().redirect("view_request.xhtml?req=" + request.getId());
    }

    public boolean taskStatusActive(Task task) {
        return task.getActiveStatus().equals(ActiveStatus.ACTIVE);
    }

    public void filter() throws ParseException {

        if (filteredTask == null && filteredEmployee == null) {
            requestList = requestFacade.findAll();
            requestListForUser = requestFacade.findAllByEmployee(currentEmployee);
        } else if (filteredTask != null && filteredEmployee != null) {
            requestList = requestFacade.findAllByTaskAndEmployee(filteredTask, filteredEmployee);
        } else if (filteredTask != null && filteredEmployee == null) {
            requestList = requestFacade.findAllByTask(filteredTask);
            requestListForUser = requestFacade.findAllByTaskAndEmployee(filteredTask, currentEmployee);
        } else if (filteredTask == null && filteredEmployee != null) {
            requestList = requestFacade.findAllByEmployee(filteredEmployee);
        }

        List<Request> requestListTemp = new ArrayList<>();
        List<Request> requestListForUserTemp = new ArrayList<>();

        if (filteredStatus != null) {
            for (Request request : requestList) {
                if (!request.getStatus().equals(filteredStatus)) {
                    requestListTemp.add(request);
                }
            }
            for (Request request : requestListForUser) {
                if (!request.getStatus().equals(filteredStatus)) {
                    requestListForUserTemp.add(request);
                }
            }
        }

        if (filteredDate != null) {
            Date formattedDate = formattedDateInDate(filteredDate);
            for (Request request : requestList) {
                if (!request.getRequestDate().equals(formattedDate)) {
                    requestListTemp.add(request);
                }
            }
            for (Request request : requestListForUser) {
                if (!request.getRequestDate().equals(formattedDate)) {
                    requestListForUserTemp.add(request);
                }
            }

        }
        requestList.removeAll(requestListTemp);
        requestListForUser.removeAll(requestListForUserTemp);
    }

    public List<Request> getRequestList() {
        return requestList;
    }

    public void setRequestList(List<Request> requestList) {
        this.requestList = requestList;
    }

    public List<Request> getRequestListForUser() {
        return requestListForUser;
    }

    public void setRequestListForUser(List<Request> requestListForUser) {
        this.requestListForUser = requestListForUser;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Task getTaskForNewRequest() {
        return taskForNewRequest;
    }

    public void setTaskForNewRequest(Task taskForNewRequest) {
        this.taskForNewRequest = taskForNewRequest;
    }

    public Task getFilteredTask() {
        return filteredTask;
    }

    public void setFilteredTask(Task filteredTask) {
        this.filteredTask = filteredTask;
    }

    public Employee getFilteredEmployee() {
        return filteredEmployee;
    }

    public void setFilteredEmployee(Employee filteredEmployee) {
        this.filteredEmployee = filteredEmployee;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public String getFilteredStatus() {
        return filteredStatus;
    }

    public void setFilteredStatus(String filteredStatus) {
        this.filteredStatus = filteredStatus;
    }

    public List<String> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<String> statusList) {
        this.statusList = statusList;
    }

    public Date getFilteredDate() {
        return filteredDate;
    }

    public void setFilteredDate(Date filteredDate) {
        this.filteredDate = filteredDate;
    }

}
