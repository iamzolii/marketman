/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import facade.WarehouseFacade;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import model.ActiveStatus;
import model.Warehouse;

/**
 *
 * @author bzolt
 */
@Named(value = "warehouseController")
@ViewScoped
public class WarehouseController extends UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612706L;

    @EJB
    WarehouseFacade warehouseFacade;

    private List<Warehouse> warehouseList;
    private Warehouse warehouse;

    private boolean showInactive;
    private String filteredAddress;
    private String filteredName;

    public WarehouseController() {
    }

    public void init() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            filter();
        }
    }
    
    public void permanentDelete(Warehouse warehouse) throws IOException {
        if (warehouse.getItemList().isEmpty()) {
            warehouseFacade.remove(warehouse);
            if (warehouseList != null && !warehouseList.isEmpty() && warehouseList.contains(warehouse)) {
                warehouseList.remove(warehouse);
                addMessage("Delete successful");
            }
            else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("warehouses.xhtml");
            }
        } else {
            addErrorMessage("Warehouse can't be deleted because there are items in it.");
        }
    }
    
    public void filter() {
        if (showInactive) {
            warehouseList = warehouseFacade.findAll();
        } else {
            warehouseList = warehouseFacade.findAllActive();
        }
        List<Warehouse> tempWarehouseList = new ArrayList<>();
        if (filteredName != null) {
            if (!filteredName.isEmpty()) {
                for (Warehouse wh : warehouseList) {
                    if (!wh.getName().toUpperCase().contains(filteredName.toUpperCase())) {
                        tempWarehouseList.add(wh);
                    }
                }
            }
        }
        if (filteredAddress != null) {
            if (!filteredAddress.isEmpty()) {
                for (Warehouse wh : warehouseList) {
                    if (!wh.getAddress().toUpperCase().contains(filteredAddress.toUpperCase())) {
                        tempWarehouseList.add(wh);
                    }
                }
            }
        }
        warehouseList.removeAll(tempWarehouseList);
    }

    public void addNewWarehouse() throws IOException {
        warehouseFacade.create(warehouse);
        FacesContext.getCurrentInstance().getExternalContext().redirect("warehouses.xhtml");
    }

    public void deleteWarehouse(Warehouse warehouse) throws IOException {
        warehouse.setActiveStatus(ActiveStatus.INACTIVE);
        if (warehouseList != null && !warehouseList.isEmpty() && warehouseList.contains(warehouse)) {
            warehouseList.remove(warehouse);
        }
        warehouseFacade.edit(warehouse);
        addMessage("Delete successful");
    }

    public ActiveStatus[] allActiveStatus() {
        return ActiveStatus.values();
    }

    public String activeStyleColor() {
        if (warehouse.getActiveStatus().equals(ActiveStatus.ACTIVE)) {
            return "green";
        }
        return "red";
    }

    public boolean warehouseStatusActive(Warehouse warehouse) {
        return warehouse.getActiveStatus().equals(ActiveStatus.ACTIVE);
    }

    public void editWarehouse() throws IOException {
        if (warehouse.getActiveStatus().equals(ActiveStatus.INACTIVE)) {
            deleteWarehouse(warehouse);
        }
        warehouseFacade.edit(warehouse);
        FacesContext.getCurrentInstance().getExternalContext().redirect("view_warehouse.xhtml?warehouse=" + warehouse.getId());
    }

    public void initExistingWarehouse() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();
            String warehouseId = params.get("warehouse");
            warehouse = warehouseFacade.findById(Integer.parseInt(warehouseId));
        }
    }

    public boolean activityRendered(Warehouse warehouse) {
        return warehouse.getActiveStatus().equals(ActiveStatus.INACTIVE);
    }

    public void initNewWarehouse() throws IOException {
        if (getLoggedInUser() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else if (getLoggedInUser().getId() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } else {
            warehouse = new Warehouse();
        }
    }

    public List<Warehouse> getWarehouseList() {
        return warehouseList;
    }

    public void setWarehouseList(List<Warehouse> warehouseList) {
        this.warehouseList = warehouseList;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public boolean isShowInactive() {
        return showInactive;
    }

    public void setShowInactive(boolean showInactive) {
        this.showInactive = showInactive;
    }

    public String getFilteredAddress() {
        return filteredAddress;
    }

    public void setFilteredAddress(String filteredAddress) {
        this.filteredAddress = filteredAddress;
    }

    public String getFilteredName() {
        return filteredName;
    }

    public void setFilteredName(String filteredName) {
        this.filteredName = filteredName;
    }

}
