function show(filterId, buttonId1, buttonId2) {
    if (document.getElementById(filterId).style.display == "none") {
        document.getElementById(filterId).style.display = "inline";
        document.getElementById(buttonId1).style.display = "none";
        document.getElementById(buttonId2).style.display = "inline";
    } else {
        document.getElementById(filterId).style.display = "none";
        document.getElementById(buttonId2).style.display = "none";
        document.getElementById(buttonId1).style.display = "inline";
    }
}
