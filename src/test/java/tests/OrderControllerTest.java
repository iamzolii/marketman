/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import control.OrderController;
import control.UserController;
import facade.ItemFacade;
import facade.MarketOrderFacade;
import java.math.BigDecimal;
import java.text.ParseException;
import model.Item;
import model.MarketOrder;
import model.OrderItem;
import model.OrderStatus;
import model.OrderType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import static org.powermock.api.support.membermodification.MemberMatcher.constructor;
import static org.powermock.api.support.membermodification.MemberModifier.suppress;
import org.powermock.core.classloader.annotations.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * //// * @author bzolt
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(OrderController.class)
public class OrderControllerTest {

    public OrderControllerTest() {
    }
    
    @Mock
    MarketOrderFacade orderFacade;
    
    @Mock
    ItemFacade itemFacade;
    
    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCompleteOrderStatus() throws ParseException {
        suppress(constructor(OrderController.class));
        PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(UserController.class));
        OrderController orderController = new OrderController();
        
        MarketOrder order = new MarketOrder();
        OrderItem orderItem1 = new OrderItem();
        Item item1 = new Item();

        item1.setQuantity(1);
        item1.setUnitPrice(10);
        orderItem1.setOrder(order);
        orderItem1.setItem(item1);
        order.getOrderItemList().add(orderItem1);
        item1.getOrderItemList().add(orderItem1);
        order.setOrderType(OrderType.CUSTOMER);
        
        orderController.setOrderFacade(orderFacade);
        
        orderController.completeOrder(order);
        Assert.assertEquals(order.getStatus(), OrderStatus.DONE);
    }
    
    @Test
    public void testCompleteCustomerOrderItemQuantities() throws ParseException {
        suppress(constructor(OrderController.class));
        PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(UserController.class));
        OrderController orderController = new OrderController();
        
        MarketOrder order = new MarketOrder();
        OrderItem orderItem1 = new OrderItem();
        Item item1 = new Item();

        item1.setQuantity(1);
        item1.setUnitPrice(10);
        orderItem1.setOrder(order);
        orderItem1.setItem(item1);
        order.getOrderItemList().add(orderItem1);
        item1.getOrderItemList().add(orderItem1);
        order.setOrderType(OrderType.CUSTOMER);
        
        orderController.setOrderFacade(orderFacade);
        
        orderController.completeOrder(order);
        Assert.assertEquals(new BigDecimal(item1.getQuantity()), new BigDecimal(1l));
    }
    
    @Test
    public void testCompleteSupplierOrderItemQuantities() throws ParseException {
        suppress(constructor(OrderController.class));
        PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(UserController.class));
        OrderController orderController = new OrderController();
        
        MarketOrder order = new MarketOrder();
        OrderItem orderItem1 = new OrderItem();
        Item item1 = new Item();

        item1.setQuantity(1);
        item1.setUnitPrice(10);
        orderItem1.setOrder(order);
        orderItem1.setItem(item1);
        orderItem1.setQuantity(1);
        order.getOrderItemList().add(orderItem1);
        item1.getOrderItemList().add(orderItem1);
        order.setOrderType(OrderType.SUPPLIER);
        
        orderController.setOrderFacade(orderFacade);
        orderController.setItemFacade(itemFacade);
        
        orderController.completeOrder(order);
        Assert.assertEquals(new BigDecimal(item1.getQuantity()), new BigDecimal(2l));
    }
    
    @Test
    public void testDeleteOrderStatus() throws ParseException {
        suppress(constructor(OrderController.class));
        PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(UserController.class));
        OrderController orderController = new OrderController();
        
        MarketOrder order = new MarketOrder();
        OrderItem orderItem1 = new OrderItem();
        Item item1 = new Item();

        item1.setQuantity(1);
        item1.setUnitPrice(10);
        orderItem1.setOrder(order);
        orderItem1.setItem(item1);
        order.getOrderItemList().add(orderItem1);
        item1.getOrderItemList().add(orderItem1);
        order.setOrderType(OrderType.SUPPLIER);
        
        orderController.setOrderFacade(orderFacade);
        
        orderController.deleteOrder(order);
        Assert.assertEquals(order.getStatus(), OrderStatus.STORNO);
    }
    
    @Test
    public void testDeleteSupplierOrderItemQuantities() throws ParseException {
        suppress(constructor(OrderController.class));
        PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(UserController.class));
        OrderController orderController = new OrderController();
        
        MarketOrder order = new MarketOrder();
        OrderItem orderItem1 = new OrderItem();
        Item item1 = new Item();

        item1.setQuantity(1);
        item1.setUnitPrice(10);
        orderItem1.setOrder(order);
        orderItem1.setItem(item1);
        orderItem1.setQuantity(1);
        order.getOrderItemList().add(orderItem1);
        item1.getOrderItemList().add(orderItem1);
        order.setOrderType(OrderType.SUPPLIER);
        
        orderController.setOrderFacade(orderFacade);
        orderController.setItemFacade(itemFacade);
        
        orderController.deleteOrder(order);
        Assert.assertEquals(new BigDecimal(item1.getQuantity()), new BigDecimal(1l));
    }
    
    @Test
    public void testDeleteCustomerOrderItemQuantities() throws ParseException {
        suppress(constructor(OrderController.class));
        PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(UserController.class));
        OrderController orderController = new OrderController();
        
        MarketOrder order = new MarketOrder();
        OrderItem orderItem1 = new OrderItem();
        Item item1 = new Item();

        item1.setQuantity(1);
        item1.setUnitPrice(10);
        orderItem1.setOrder(order);
        orderItem1.setItem(item1);
        orderItem1.setQuantity(1);
        order.getOrderItemList().add(orderItem1);
        item1.getOrderItemList().add(orderItem1);
        order.setOrderType(OrderType.CUSTOMER);
        
        orderController.setOrderFacade(orderFacade);
        orderController.setItemFacade(itemFacade);
        
        orderController.deleteOrder(order);
        Assert.assertEquals(new BigDecimal(item1.getQuantity()), new BigDecimal(2l));
    }
}
